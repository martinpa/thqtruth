# tHqTruth

## 1. Building
To build the project:
```
setupATLAS
export RUCIO_ACCOUNT=$USER
lsetup "asetup AnalysisBase,21.2.170,here" git pyami panda
#lsetup "rucio 1.21.11.patch1"
git atlas init-workdir -b 21.2 https://:@gitlab.cern.ch:8443/atlas/athena.git
cd athena
git clone -b v33_ll https://:@gitlab.cern.ch:8443/atlasphys-top/singletop/SingleTopAnalysis.git # Note that you may want to do other tag appart from the -b v33 
git clone ssh://git@gitlab.cern.ch:7999/nbruscin/tthbbutils.git
#git checkout release/21.2.163
git atlas addpkg TopPartons
git atlas addpkg TopAnalysis
git atlas addpkg TopConfiguration
# We must copy our version (this part may not be necessary)
cd PhysicsAnalysis/TopPhys/xAOD/ 
git clone  ssh://git@gitlab.cern.ch:7999/martinpa/thqtruth.git
rm -rf TopPartons/ && mv thqtruth/TopPartons/ ./TopPartons && rm -rf thqtruth/
cd ../../../ 
mkdir -p ../build && cd ../build
cmake ../athena/Projects/WorkDir
cmake --build ./
source */setup.sh
mkdir -p ../run; cd ../run
```
### 1.1 Not running from lxplus
If, instead of running at CERN `lxplus`, you are working from your institute account, for instance `sctui`, you would need to execute the following additional commands
```
kinit martinpa@CERN.CH
aklog CERN.CH
export RUCIO_ACCOUNT=martinpa
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS
```
at `sctui` you may need to specify your CERN user to initialise athena
```
git atlas init-workdir -b 21.2 https://:@gitlab.cern.ch:8443/martinpa/athena.git
```
this instruction specify the fork you want to download

### 1.2 Checkout other branch
If you want to make modifications to the main version of the code (21.2 currently), you should create a new branch ([instructions](https://docs.gitlab.com/ee/gitlab-basics/create-branch.html)), switch to that branch

```
setupATLAS
export RUCIO_ACCOUNT=$USER
lsetup "asetup AnalysisBase,21.2.170,here" git pyami panda
git atlas init-workdir -b 21.2 https://:@gitlab.cern.ch:8443/atlas/athena.git
cd athena
git clone -b v33_ll https://:@gitlab.cern.ch:8443/atlasphys-top/singletop/SingleTopAnalysis.git
git clone ssh://git@gitlab.cern.ch:7999/nbruscin/tthbbutils.git
git checkout TopPartons_Fixes_for_tHq # TopPartons_Fixes_for_tHq is the name of the branch
git atlas addpkg TopPartons
git atlas addpkg TopAnalysis
git atlas addpkg TopConfiguration
mkdir -p ../build && cd ../build
cmake ../athena/Projects/WorkDir
cmake --build ./
source */setup.sh
mkdir -p ../run; cd ../run

``` 

apply your modifications to the new branch and, once you have tested them, do a Merge Request

## 2. Clean setup on lxplus

Log into lxplus and go to the directory you installed the [4TruthHistos](https://gitlab.cern.ch/atlasphys-top/singletop/SingleTopAnalysis/) package (following the previous steps). Then, setup the AnalysisBase release you want to use (i.e. find out your preferred X from the [AnalysisTop 21.2](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisTop21) twiki).

```
setupATLAS --quiet
export RUCIO_ACCOUNT=$USER
voms-proxy-init -voms atlas
lsetup "asetup AnalysisBase,21.2.170,here" git pyami rucio panda
cd build
source */setup.sh
cd ../run
```

## 3. Executing 
### Over a sample
To run over a sample, create the file `in.txt` with the path to the sample that we want to use. For isntance, a tH inclusive sample file is  `/eos/atlas/user/c/cescobar/top/data/DAOD_TOPQ1/mc16_13TeV.346676.aMcAtNloPythia8EvtGen_tHjb125_4fl_CPalpha_0.deriv.DAOD_TOPQ1.e7815_a875_r10201_p4174/DAOD_TOPQ1.21697035._000046.pool.root.1` or a more recent one `/lhome/ific/c/cescobar/lustre/data/TOPQ1/mc16_13TeV.346676.aMcAtNloPythia8EvtGen_tHjb125_4fl_CPalpha_0.deriv.DAOD_TOPQ1.e7815_a875_r9364_p4346/DAOD_TOPQ1.24248477._000008.pool.root.1`

For running over 100 events do 
```
local_run.sh --partonlevel-truthinfo --jet-collection=EM --no-systematics --channel ll --maxevents 100 --tau
```

Other option. Instead of creating the in.txt as decribed above, from the run directory create a link to the test samples located in ../athena/SingleTopAnalysis/test
```
ln -sf test/in_mc16_13TeV.346799.aMcAtNloPythia8EvtGen_tHjb125_4fl_CPalpha_0_ML_2L.deriv.DAOD_TOPQ1.e8018_a875_r9364_p4346.txt in.txt
```

### Over complete sample
In order to execute TopPartons over the entire sample, the job must be sent to the grid. First of all you clean your run directory <!--- because there may be softlinks that lead to a crash --> and configure the `SubmitToGrid.py `. Firstly, you must check that your name appears in the findSE function, then desable the `config.particleLevelList` for the sample you plan to submit (346676 for the tHq inclusive) if you are only interested in the parton level, here you can also change the `config.suffix`  and many other options. In the `MCSamples.py` you can check the name to be used when submiting to grid by looking at the header `SingleTopAnalysis.grid.Add("NameOfInterest")` that goes with the DSID of your interest<!--- (for 346676 you have mc16e_13TeV_tWHq)--> and comment the entries for all the other DSID that appear in that list of datasets. Finally execute the `SubmitToGrid.py` 

```
rm *
emacs -nw ../athena/SingleTopAnalysis/scripts/SubmitToGrid.py #check and configure
emacs MCSamples.py -nw #Check the NameOfInterest in the SingleTopAnalysis.grid.Add that goes with your DSID
SubmitToGrid.py NameOfInterest #for instance: mc16e_13TeV_tWHq
```
Check the status of your job at panda https://bigpanda.cern.ch/ <!--- https://bigpanda.cern.ch/user/?user=Pablo%20Martinez%20Agullo -->

## 4. Recompiling 
For recompiling when modifying the .h files. From run directory:
```
rm -rf ../build/; mkdir -p ../build && cd ../build; cmake ../athena/Projects/WorkDir; cmake --build ./; source */setup.sh; cd ../run
```
if not .h file has been modified we can simply do:
```
cd ../build; cmake ../athena/Projects/WorkDir; cmake --build ./; source */setup.sh; cd ../run
```
